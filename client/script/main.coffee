stage = new PIXI.Stage(0x110022)


renderer = PIXI.autoDetectRenderer(window.innerWidth, window.innerHeight, null)
document.body.appendChild(renderer.view)
renderer.view.style.position = "absolute" #full screen stuff
renderer.view.style.top = renderer.view.style.left = "0"

# resize renderer on window resize
$(window).resize( () ->
  renderer.resize(window.innerWidth, window.innerHeight)
)

input = new Game.InputManager
input.init(window)

text = new PIXI.Text("Text", {font: "bold italic 14px Arvo", fill: "#eeffee", align: "left", stroke: "#ddeeff", strokeThickness: 1})
text.position.x = text.position.y = 5
text.anchor.x = text.anchor.y = 0
stage.addChild(text)

bunnies = []
asteroids = []

onAssetsLoaded = () ->
  bunny = new Game.Bunny(200,150)
  stage.addChild(bunny.sprite)
  bunnies.push bunny

  asteroids = _.map(_.range(8), ->
    asteroid = new Game.Asteroid(100+_.random(500), 100+_.random(500))
    stage.addChild(asteroid.sprite)
    asteroid
  )

loader = new PIXI.AssetLoader(['assets/main0.json'])
loader.onComplete = onAssetsLoaded; # use callback
loader.load(); #begin load

time = null
animate = () ->
#  setTimeout(( -> requestAnimFrame( animate )),100) # Test low fps
  requestAnimFrame( animate )

  now = Date.now()
  dt = (now - (time || now)) #delta time in ms
  fps = 1000 / dt
  dt *= 0.001 #delta time in sec
  time = now

  text.setText "FPS #{parseInt(fps)} dt: #{dt.toFixed 3} "

  bunny.update(dt) for bunny in bunnies
  asteroid.update(dt) for asteroid in asteroids
  Game.Laser.updateLasers(stage, dt)

  renderer.render(stage)

requestAnimFrame( animate )
