class Game.Bunny
  speed: 300
  lastShot: null

  constructor: (x,y) ->
    @sprite = new PIXI.Sprite(Game.getTextureFromFrame("bunny"))
    @sprite.anchor.x = 0.5
    @sprite.anchor.y = 0.5
    @sprite.position.x = x
    @sprite.position.y = y

  update: (dt) ->
    @moveUp(dt) if (Game.Key.isDown(Game.Key.UP))
    @moveLeft(dt) if (Game.Key.isDown(Game.Key.LEFT))
    @moveDown(dt) if (Game.Key.isDown(Game.Key.DOWN))
    @moveRight(dt) if (Game.Key.isDown(Game.Key.RIGHT))
    @shoot() if @canShoot() && (Game.Key.isDown(Game.Key.SPACE))

  moveUp: (dt) ->
    @sprite.position.y -= @speed  * dt

  moveDown: (dt) ->
    @sprite.position.y += @speed  * dt

  moveRight: (dt) ->
    @sprite.position.x += @speed  * dt

  moveLeft: (dt) ->
    @sprite.position.x -= @speed  * dt

  shoot: ->
    Game.Laser.spawn(@sprite.position.x, @sprite.position.y)
    @lastShot = @getEpoch()

  canShoot: ->
    (@getEpoch() - @lastShot) > 250

  getEpoch: ->
    new Date().getTime()
