class Game.Laser
  constructor: (x, y) ->
    @sprite = new PIXI.Sprite(Game.getTextureFromFrame("bullet"))
    @sprite.anchor.x = 0.5
    @sprite.anchor.y = 0.5
    @sprite.rotation -= 1.57079633
    @sprite.position.x = x
    @sprite.position.y = y
    @added = false

  @updateLasers: (stage, dt) ->
    for laser in @_laserArray
      laser.sprite.position.y -= 1000 * dt

      if laser.added == false
        stage.addChild(laser.sprite)
        laser.added = true

      if laser.offScreen()
        stage.removeChild(laser.sprite)
        Game.Laser.delete(laser)

  @_laserArray: []

  @getLasers: ->
    @_laserArray

  @spawn: (x, y) ->
    @_laserArray.push new Game.Laser(x, y)

  @delete: (laser) ->
    pos = @_laserArray.indexOf(laser)
    @_laserArray.splice(pos, 1)

  offScreen: ->
    @sprite.position.y < 0
