class Game.Asteroid
  speed: 100

  constructor: (x,y) ->
    @sprite = new Game.SpriteAnimation(Game.getTexturesFromFrameBase('asteroid'), 0.5)
    @sprite.anchor.x = 0.5
    @sprite.anchor.y = 0.5
    @sprite.position.x = x
    @sprite.position.y = y

  update: (dt) ->
    @sprite.update(dt)
    @sprite.rotation += 0.5 * dt
