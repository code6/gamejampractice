describe 'test', ->
  it 'should know true is true', ->
    expect(true).toBe(true)

  it 'should be a bunny', ->
    loader = new PIXI.AssetLoader(['assets/main0.json'])
    loader.load(); #begin load
    bunny = new Game.Bunny(5,16)
    expect(bunny.sprite.position.x).toBe(5)
