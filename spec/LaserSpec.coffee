describe 'Game.Laser', ->
  # Instance methods
  describe 'constructor', ->
    beforeEach ->
      @laser = new Game.Laser(0, 0)

    it 'construct', ->
      expect(@laser).toNotBe( null )
    it 'added should be false', ->
      expect(@laser.added).toBe( false )

  it 'is offScreen', ->
    @laser = new Game.Laser(0, -1)
    expect( @laser.offScreen() ).toBeTruthy()

  # Class methods
  it 'spawns', ->
    expect(Game.Laser.spawn).toBeDefined()

  it 'get array of lasers', ->
    expect(Game.Laser.getLasers).toBeDefined()

  describe 'updateLasers', ->
    beforeEach ->
      @stageStub =
        addChild: (laser) ->
          null
        removeChild: (laser) ->
          null
    it 'removes offScreen lasers', ->
      Game.Laser.spawn(0, -1)
      Game.Laser.updateLasers(@stageStub, 0)
      expect( Game.Laser.getLasers().length ).toBe( 0 )
