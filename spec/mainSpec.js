(function() {
  describe('test', function() {
    it('should know true is true', function() {
      return expect(true).toBe(true);
    });
    return it('should be a bunny', function() {
      var bunny, loader;
      loader = new PIXI.AssetLoader(['assets/main0.json']);
      loader.load();
      bunny = new Game.Bunny(5, 16);
      return expect(bunny.sprite.position.x).toBe(5);
    });
  });

}).call(this);
