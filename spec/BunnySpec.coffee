describe 'bunny', ->
  describe 'shooting', ->
    beforeEach () ->
      @bunny = new Game.Bunny

    it 'shoot defined', ->
      expect(@bunny.shoot).toBeDefined()

    it 'shoot define last shot', ->
      expect(@bunny.lastShot).toBeDefined()

    it 'can Shoot', ->
      expect(@bunny.canShoot()).toBeTruthy()

    it 'can not shoot after having just shot', ->
      @bunny.lastShot = @bunny.getEpoch()
      expect(@bunny.canShoot()).toBeFalsy()
